<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\fixorder;
use Illuminate\Support\Facades\Auth;
use DB;
use Mail;
use PHPMailer\PHPMailer\PHPMailer;
class cartcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if ( Auth::check() )
        {
            $data = DB::table('carts')
                ->where('carts.id_buy', Auth::user()->name )
                ->leftjoin('products','carts.id_cart', '=' , 'products.id')
                // ->join('products','carts.price', '=' , 'products.Price')
                // ->join('products','carts.id_cart', '=' , 'products.QTY')
                // ->join('products','carts.id_cart', '=' , 'products.photo')
                // ->join('products','carts.id_cart', '=' , 'products.Size')
                // ->join('products','carts.id_cart', '=' , 'products.Color')
                // ->select('*')
                ->get();
                
            // return $data;
        return view('frontend.cart.cart', compact('data'));        
        }
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cart = Cart::all(); 
        // $select = '';

        // foreach ($carty as $key => $value) {
        //     $select .= '<li> <a href="#">'.$value->CategoryName.'</a></li>'    ;
        // }
        
        
        

        return $cart;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           $cart = $request->all();
           
        // dd($cart);
        $crt = new Cart();
        $crt->id_buy = Auth::user()->name;
        $crt->id_cart = $request->id;
        $crt->qty = $request->qty;
        $crt->size = $request->size;
        $crt->color = $request->color;
        $crt->price = $request->price;
        $crt->save();
        $data = Cart::all()->where('id_buy', Auth::user()->name)->count();

       
        if ($crt){
            return response()->json([
            'success' => true,
            'message' => 'Cart Created',
            'data' => $data, 
        ]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deletebutton($id)
    {
 
            $cart = Cart::where('id_cart',$id)->where('id_buy', Auth::user()->name)->first();
            $cart->delete();

        return response()->json([
            'success' => true,
            'message' => 'Cart Deleted'
        ]);
    }
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list_cart()
    {
        if ( Auth::check() )
        {
            $data = DB::table('carts')
                ->where('carts.id_buy', Auth::user() )
                ->leftJoin('products','products.ProductName', '=' , 'carts.id_cart')
                ->leftJoin('products','products.Price', '=' , 'carts.price')
                ->leftJoin('products','products.QTY', '=' , 'carts.qty')
                ->leftJoin('products','products.photo', '=' , 'carts.photo')
                ->leftJoin('products','products.Size', '=' , 'carts.size')
                ->leftJoin('products','products.Color', '=' , 'carts.color')
                ->get();
                dd($data);
            return $data;
                
        }
    }

    public function checkout()
    {

         if ( Auth::check() )
        {
            $data = DB::table('carts')
                ->where('carts.id_buy', Auth::user()->name )
                ->leftjoin('products','carts.id_cart', '=' , 'products.id')
                // ->join('products','carts.price', '=' , 'products.Price')
                // ->join('products','carts.id_cart', '=' , 'products.QTY')
                // ->join('products','carts.id_cart', '=' , 'products.photo')
                // ->join('products','carts.id_cart', '=' , 'products.Size')
                // ->join('products','carts.id_cart', '=' , 'products.Color')
                // ->select('*')
                ->get();

            // $dat = new fixorder();
            // $dat->barang = DB::table('carts')
            //     ->where('carts.id_buy', Auth::user()->name )->select('id_cart')->get();
            // $dat->save();    
            // return $data;
        
        return view('frontend.cart.checkout',compact('data'));
        }
    }

    public function complete(Request $request)
    {
        $orderr = $request->all();
        // return $request->kota;

                $client = new \GuzzleHttp\Client([
                    'headers' => [ 'Content-Type' => 'application/json' ,
                                'key' => 'f214df40871ea3e93f87058943919b31']
                                                ]);
                $response = $client->request('POST', 'http://api.rajaongkir.com/starter/cost', [
                    'form_params' => [
                
                'origin' => '23',
                'destination' => $request->kota,
                'weight' => '1',
                'courier' => 'jne'
                         ]
                    ]);
                $responses = $response->getBody();
                $json = json_decode($responses,true);
                $ongkir = $json['rajaongkir']['results'];
                $var =  response()->json(['data'=> $ongkir]);
                $select = '';
                $final = [];
                $blah = '';
                $blah2 = '';
                $blah3 = '';
                foreach ($json['rajaongkir']['results'] as $key => $value) {

                $select = $value['costs'];
                foreach ($select as $key => $val) {
                    $blah .= $val['description'];
                    $blah2 .= $val['service'];
                    $blah3 .= json_encode($val['cost']);
                    // $blah3 .= $val['cost'];
                $explode_blah3 = explode("{}", $blah3);
                $ex = explode(":", $explode_blah3[0]); 
                $int = preg_replace('/[^0-9]/', '', $ex);
                }
                // $select .= $value['province_id'] ; 
                // dd($select);
                    // foreach ($select as $key => $final) {
                    //     $blah .= $final['service'];
                    // }
                    // foreach ($select as $key => $final2) {
                    //     $blah2 = $final2['service'];
                    // }
                $final['desc'] = $blah;
                $final['ser'] = $blah2;
                $final['ongkir'] = $blah3;
                $final['opsi1'] = $int[1];
                $final['opsi2'] = $int[4];


                return response()->json($final);
                return $int;
                     
                 }
                // return response()->json($select);
                
                                
    }

    public function com(Request $request)
    {
        $data = $request->all();
        $fixed = fixorder::create($data);
        $fixed->save();

        
                //Create a new PHPMailer instance
        $mail = new PHPMailer;
        // Set PHPMailer to use the sendmail transport
        // $mail->isSendmail();
        // $mail->CharSet = 'UTF-8';
        // $mail->SMTPAuth = 'true';
        // $mail->SMTPSecure = "tls";
        // $mail->Host = "smtp.gmail.com";
        // $mail->Port = 587;
        // $mail->Username = "ozzy.ojjey@gmail.com";
        // $mail->Password = "hayobingungya";
        // //Set who the message is to be sent from
        // $mail->setFrom('fauzifadillah00@yahoo.com', 'Fauzi Fadillah');
        // $mail->Subject = 'PHPMailer sendmail test';
        // $mail->Body = 'TES AJA';
        // //Set an alternative reply-to address
        // $mail->addReplyTo('replyto@example.com', 'First Last');
        // //Set who the message is to be sent to
        // $mail->addAddress('ozzy.ojjey@gmail.com', 'Fauzi Fadillah');
        // $mail->send();

        $mail->isSMTP();                            // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';             // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                     // Enable SMTP authentication
        $mail->Username = 'ozzy.ojjey@gmail.com';          // SMTP username
        $mail->Password = 'hayobingungya'; // SMTP password
        $mail->Port = 465;                          // TCP port to connect to
        $mail->SMTPSecure = 'ssl';                  // Enable TLS encryption, `ssl` also accepted

        $mail->setFrom('ozzy.ojjey@gmail.com', 'Lumea Orders');
        $mail->addReplyTo('info@example.com', 'CodexWorld');
        $mail->addAddress($request->email);   // Add a recipient
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        $mail->isHTML(true);  // Set email format to HTML

        $bodyContent = '<h1>Terimakasih Sudah berbelanja di Lumea!</h1>';
        $bodyContent .= '<p>Transaksi anda akan segera kami proses</b></p>';
        $bodyContent .= '<p> Nama : '.$request->name.' </p>';
        $bodyContent .= '<p> Alamat : '.$request->alamat.' </p> ';
        $bodyContent .= '<p> Kontak : '.$request->no_hp.' </p>';
        $bodyContent .= '<p> Bank Pembayaran : '.$request->metode.' </p>';
        $bodyContent .= '<p> Total : Rp. '.$request->harga.' </p>';
        $bodyContent .= '<p> KONFIRMASI PEMBAYARAN KE : 085947224853 </p>';
        $bodyContent .= '<p> ---------- TERIMAKASIH ---------- </p>';

        $mail->Subject = 'LUMEA';
        $mail->Body    = $bodyContent;

        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
        
       
        //Set the subject line
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        // $mail->msgHTML(file_get_contents('contents.html'), __DIR__);
        //Replace the plain text body with one created manually
        // $mail->AltBody = 'This is a plain-text message body';
        //Attach an image file
        // $mail->addAttachment('images/phpmailer_mini.png');
        //send the message, check for errors
        // if (!$mail->send()) {
        //     echo "Mailer Error: " . $mail->ErrorInfo;
        // } else {
        //     echo "Message sent!";
        // }
    }

   
}
